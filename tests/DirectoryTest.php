<?php

namespace JontyNewman\Oku\Tests;

use DomainException;
use JontyNewman\Oku\File;
use JontyNewman\Oku\Directory;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SplFileInfo;

require 'vendor/symfony/filesystem/Filesystem.php';

class DirectoryTest extends TestCase
{
	public function test()
	{
		$this->assertDirectory();
	}

	public function testWithExtension()
	{
		$this->assertDirectory('txt');
	}

	public function testWithInvalidExtension()
	{
		$exception = null;
		$extension = '/';

		try {
			new Directory('', $extension);
		} catch (DomainException $exception) {
			$this->assertEquals(
					"Extension '{$extension}' must be alphanumeric",
					$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
	}

	public function testWithInvalidPrefixForIteration()
	{
		$exception = null;

		try {
			iterator_to_array(new Directory(''));
		} catch (DomainException $exception) {
			$this->assertEquals(
					'Prefix must end with a single directory separator for reliable iteration',
					$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
	}

	private function assertDirectory(string $extension = null)
	{
		$exception = null;
		$root = vfsStream::setup();
		$prefix = "{$root->url()}/";
		$offset = '/';
		$value = 'Hello, world!';
		$directory = new Directory($prefix, $extension);
		$encoded = File::path($offset);
		$path = "{$prefix}{$encoded}";

		if (!is_null($extension)) {
			$path .= ".{$extension}";
		}

		$this->assertEquals($path, $directory->offsetGet(''));
		$this->assertFalse($directory->offsetExists($offset));

		$this->assertNotFalse(file_put_contents($path, ''));
		$this->assertTrue(chmod($path, 0));

		try {
			$directory->offsetSet($offset, $value);
		} catch (RuntimeException $exception) {
			$this->assertEquals(
					"Cannot put contents to '{$path}'",
					$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$this->assertTrue(unlink($path));

		$directory->offsetSet($offset, $value);

		$this->assertTrue($directory->offsetExists($offset));
		$this->assertEquals($value, file_get_contents($path));

		$callback = function (SplFileInfo $finfo) {
			return $finfo->getPathname();
		};
		$files = array_map($callback, iterator_to_array($directory));
		$this->assertSame([$offset => $path], $files);

		$directory->offsetUnset($offset);
		$this->assertFalse($directory->offsetExists($offset));

		$invalid = "{$prefix}{$encoded}";

		if (is_null($extension)) {
			$invalid .= '.txt';
		}

		$this->assertTrue(touch($invalid));

		$this->assertSame([], iterator_to_array($directory));
	}
}
