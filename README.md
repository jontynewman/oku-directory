# JontyNewman\Oku\Directory

Functionality for mapping arbitrary keys to file paths with a given prefix.

## Installation

```
composer require 'jontynewman/oku-directory ^1.0'
```

