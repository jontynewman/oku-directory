<?php

namespace JontyNewman\Oku;

use ArrayAccess;
use DomainException;
use Exception;
use FilesystemIterator;
use FilterIterator;
use Iterator;
use JontyNewman\Oku\File;
use LogicException;
use RuntimeException;
use SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;

/**
 * An implicit mapping of arbitrary keys to files with a given prefix.
 */
class Directory implements ArrayAccess, Iterator
{
	/**
	 * The current prefix of file paths.
	 *
	 * @var string
	 */
	private $prefix;

	/**
	 * The suffix being appended to file paths.
	 *
	 * @var string
	 */
	private $suffix;

	/**
	 * The iterator being used to traverse the directory (if any).
	 *
	 * @var \FilesystemIterator
	 */
	private $iterator;

	/**
	 * Constructs an implicit mapping of arbitrary keys to files with a given
	 * prefix.
	 *
	 * @param string $prefix The prefix to prepend to file paths.
	 * @param string|null $extension The extension to append to file paths (if
	 * any).
	 */
	public function __construct(string $prefix, string $extension = null)
	{
		$this->suffix = self::toSuffix($extension);
		$this->prefix = $prefix;
	}

	/**
	 * Determines whether or not the file associated with the given key exists.
	 *
	 * @param scalar $offset The key to use in order to determines whether or
	 * not the associated file exists.
	 * @return bool Whether or not the file associated with the given key
	 * exists.
	 */
	public function offsetExists($offset): bool
	{
		return file_exists($this->offsetGet($offset));
	}

	/**
	 * Converts the given key to the associated file path.
	 *
	 * @param scalar $offset The key to convert.
	 * @return string The converted key.
	 */
	public function offsetGet($offset)
	{
		$path = (string) $offset;

		if ('' === $path) {
			$path = '/';
		}

		$encoded = File::path($path);

		return "{$this->prefix}{$encoded}{$this->suffix}";
	}

	/**
	 * Puts the given value to the file associated with the given key.
	 *
	 * @param scalar $offset The key of the associated file.
	 * @param resource|string $value The value to put to the associated file.
	 * @throws \RuntimeException The value cannot be put to the associated file.
	 */
	public function offsetSet($offset, $value): void
	{
		$exception = null;
		$bytes = false;
		$path = $this->offsetGet($offset);

		if (!is_resource($value)) {
			$value = (string) $value;
		}

		try {
			$bytes = file_put_contents($path, $value);
		} catch (Exception $exception) {
			// Do nothing (implicitly assign the exception to the variable).
		}

		if (false === $bytes) {
			throw new RuntimeException(
				"Cannot put contents to '{$path}'",
				0,
				$exception
			);
		}
	}

	/**
	 * Removes the file associated with the given key.
	 *
	 * @param scalar $offset The key of the associated file.
	 */
	public function offsetUnset($offset): void
	{
		(new Filesystem())->remove($this->offsetGet($offset));
	}

	public function current()
	{
		return $this->iterator()->current();
	}

	public function key()
	{
		$matches = [];
		$path = $this->current();
		$prefix = preg_quote($this->prefix, '/');
		$suffix = preg_quote($this->suffix, '/');

		preg_match("/^{$prefix}(.*?){$suffix}$/", $path, $matches);

		if (!isset($matches[1])) {
			throw new LogicException("Cannot extract encoded name from file path '{$path}'"); // @codeCoverageIgnore
		}

		return File::resolve($matches[1]);
	}

	public function next(): void
	{
		$this->iterator()->next();
	}

	public function rewind(): void
	{
		$this->iterator()->rewind();
	}

	public function valid(): bool
	{
		return $this->iterator()->valid();
	}

	/**
	 * Converts the given extension to the suffix.
	 *
	 * @param string|null $extension The extension to convert.
	 * @return string The converted extension.
	 * @throws \DomainException The given extension is not alphanumeric.
	 */
	private static function toSuffix(?string $extension): string
	{
		$suffix = '';

		if (!is_null($extension)) {

			if (1 !== preg_match('/^[[:alnum:]]*$/', $extension)) {
				throw new DomainException("Extension '{$extension}' must be alphanumeric");
			}

			$suffix .= ".{$extension}";
		}

		return $suffix;
	}

	/**
	 * Gets the iterator being used to traverse the directory.
	 *
	 * @return \FilterIterator The iterator being used to traverse the
	 * directory.
	 */
	private function iterator(): FilterIterator
	{
		if (is_null($this->iterator)) {
			$this->iterator = $this->toFilterIterator();
		}

		return $this->iterator;
	}

	private function toFilterIterator(): FilterIterator
	{
		$iterator = $this->toFilesystemIterator();
		$suffix = $this->suffix;

		return new class ($iterator, $suffix) extends FilterIterator {

			private $suffix;

			public function __construct(
				Iterator $iterator,
				string $suffix
			) {
				parent::__construct($iterator);
				$this->suffix = $suffix;
			}

			public function accept(): bool
			{
				return $this->matches(parent::current());
			}


			/**
			 * Determines whether or not the given file is associated with the
			 * directory.
			 *
			 * @param \SplFileInfo $file The file to evaluate.
			 * @return bool Whether or not the given file is associated with the
			 * directory.
			 */
			private function matches(SplFileInfo $file): bool
			{
				return $file->isFile() && $this->isSuffixed($file->getBasename());
			}

			/**
			 * Determines whether or not the given name has a suffix that is associated
			 * with the directory.
			 *
			 * @param string $name The name to evaluate.
			 * @return bool Whether or not the given name has a suffix that is
			 * associated with the directory.
			 */
			private function isSuffixed(string $name): bool
			{
				$suffix = $this->suffix;
				$start = 0 - strlen($suffix);

				return 0 === $start || ($start < 0 && substr($name, $start) === $suffix);
			}
		};
	}

	/**
	 * Generates an iterator in order to traverse the directory.
	 *
	 * @return \FilesystemIterator The generated iterator.
	 * @throws \DomainException The prefix is not reliable for iteration.
	 */
	private function toFilesystemIterator(): FilesystemIterator
	{
		$valid = false;
		$char = DIRECTORY_SEPARATOR;
		$quoted = preg_quote($char, '/');
		$pattern = "/[^{$quoted}]{$quoted}$/";

		if ($char !== $this->prefix) {
			$valid = (1 === preg_match($pattern, $this->prefix));
		}

		if (!$valid) {
			throw new DomainException('Prefix must end with a single directory separator for reliable iteration');
		}

		return new FilesystemIterator($this->prefix);
	}
}
